#ifndef Sender_hpp
#define Sender_hpp


#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include "kukaUDPMsg.pb.h"
#include "Message_Queue.h"



using boost::asio::ip::udp;


class Sender

{

	boost::thread T_sender;
	bool active;
	std::string data;
	Message_Queue<std::string> *Qu;
	boost::asio::io_service io_service;
	udp::socket socket;
	udp::endpoint uep;
	

public:
	Sender() :data(""), active(false), socket(io_service){ Qu = new Message_Queue < std::string >(); };
	~Sender();
	void SocketSetup(const char*, int);
	//void SetQueue(Message_Queue<Mex_Type> *s_qu);
	//std::string SetMessage(Mex_Type);
	void SetMessage(double, double, double, double, double, double, double, double, double, double, double, double, int);
	void Send(std::string);
	void start();
	void run();
};

#endif 