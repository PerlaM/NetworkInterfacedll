// NetworkInterfacedll.cpp : Defines the exported functions for the DLL application.
//
#include "stdafx.h"
# include <iostream>
# include "Sender.h"
# include "Receiver.h"
# include "NetworkInterfacedll.h"


Sender* S= new Sender();
Receiver* R= new Receiver();

int CleanUp()
{
	delete S;
	delete R;
	return 0;
}

int Setup(const char* address, int Sport, int Rport)
{
	try{
		R->SocketSetup(Rport);
		S->SocketSetup(address, Sport);
		S->start();
		R->start();
		return 0;
	}
	catch (std::exception& e)
	{
		return 1;
	}
}

int SendMessageToRobot(double px, double py, double pz, double pa, double pb, double pc, double fx, double fy, double fz, double tx, double ty, double tz, int controltype)
{
	try{
		S->SetMessage(px, py, pz, pa, pb, pc, fx, fy, fz, tx, ty, tz, controltype);
		return 0;
	}
	catch (std::exception& e)
	{
	   return 1;
	}
}


int ReceiveMessage(double &px, double &py, double &pz, double &pa, double &pb, double &pc, double &fx, double &fy, double &fz, double &tx, double &ty, double &tz, int &controltype)
{
	try{
		R->GetMessage(px, py, pz, pa, pb, pc, fx, fy, fz, tx, ty, tz, controltype);
		return 0;
	}
	catch (std::exception& e)
	{
		return 1;
	}
}
