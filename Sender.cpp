#include "stdafx.h"
# include "Sender.h"


Sender::~Sender()
{
	active = false;
	socket.close();
}

void Sender::SocketSetup(const char *a, int port)
{
	//std::string address(a);
	boost::system::error_code ec;
	uep = udp::endpoint(boost::asio::ip::address::from_string(a), port);
	//socket = udp::socket(io_service, uep);
	socket = udp::socket(io_service);
	socket.open(udp::v4(), ec);
	if (ec)
		std::cout << ec << std::endl;

}

void Sender::Send(std::string message){

	size_t len1 = message.length();
    socket.send_to(boost::asio::buffer(message, len1), uep);

	/*DEBUG PRINTING*/
	//std::cout << "length " << len1;
	//std::cout << "serial to string: " << tran.SerializeAsString();
	std::cout << "Command sent";
	//com.PrintDebugString();
	std::cout << " " << std::endl;
	/*END DEBUG PRINTING*/
}

void Sender::start() 
{
	//Qu = new Message_Queue<std::string>(); //new object queue
	active = true;
	T_sender = boost::thread([this]{this->run(); });
	T_sender.detach();
}

void Sender::run()
{
	while (active){
		//take message from queue
		Qu->wait_and_pop(data);
		//send message
		Send(data);
		
	    //if we want to put it to sleep more
		//boost::posix_time::seconds sleepTime(3);
		//boost::this_thread::sleep(sleepTime);
	}

}



void Sender::SetMessage(double x, double y, double z, double a, double b, double c, double fx, double fy, double fz, double fa, double fb, double fc, int m)
{
	
	std::string messagecom="";
	int i = 0;
	messagecom = std::to_string(m);
	kuka::Transform *tranPose = new kuka::Transform();
	kuka::Transform *tranForce = new kuka::Transform();
	kuka::Command com;
	std::string messagesent="";

	tranPose->set_x(x);
	tranPose->set_y(y);
	tranPose->set_z(z);
	tranPose->set_a(a);
	tranPose->set_b(b);
	tranPose->set_c(c);

	tranForce->set_x(fx);
	tranForce->set_y(fy);
	tranForce->set_z(fz);
	tranForce->set_a(fa);
	tranForce->set_b(fb);
	tranForce->set_c(fc);

	com.set_msg(messagecom);
	com.set_allocated_pose(tranPose);
	com.set_allocated_force(tranForce);

	messagesent = com.SerializeAsString();

	/*DEBUG PRINTING*/
	//std::cout << "com.PrintDebugString(): ";
	//com.PrintDebugString();
	//std::cout << " " << std::endl;*/
	/*END DEBUG PRINTING*/

	Qu->m_push(messagesent); //put the message in the queue
	
}