#ifndef Receiver_hpp
#define Receiver_hpp


#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include "kukaUDPMsg.pb.h"
#include "Message_Queue.h"


//#include <thread>

using boost::asio::ip::udp;


class Receiver
{
	//std::thread R_thread;
	boost::thread R_thread;
	bool active;
	Message_Queue<kuka::Command> *Qu;
	kuka::Command data;
	boost::asio::io_service io_service;
	udp::socket socket;
	udp::endpoint uep;
	

public:
	Receiver():active(false),socket(io_service){};
	~Receiver();
	void SocketSetup(int port);
	void ReceiveMessage(); //receive method
	void start();
	void run();
	//void handler(const boost::system::error_code &error);
	void GetMessage(double &px, double &py, double &pz, double &pa, double &pb, double &pc, double &fx, double &fy, double &fz, double &fa, double &fb, double &fc, int &controltype);
};

#endif