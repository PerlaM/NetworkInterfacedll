#include "stdafx.h"
#include "Receiver.h"
#include <windows.h>





Receiver::~Receiver()
{

#ifdef WIN32
	BOOL res;
	HANDLE th = R_thread.native_handle();
	res = SetThreadPriority(th, THREAD_PRIORITY_TIME_CRITICAL);
#endif

	active = false;
	socket.close();
}

void Receiver::SocketSetup(int port)
{
	
	uep = udp::endpoint(udp::v4(), port);
	socket = udp::socket(io_service, uep);
	//socket.bind(uep);

}

void Receiver::GetMessage(double &px, double &py, double &pz, double &pa, double &pb, double &pc, double &fx, double &fy, double &fz, double &fa, double &fb, double &fc, int &controltype)
{

	
	//Qu->wait_and_pop(data);
	bool Try = Qu->try_pop(data);
	if (Try)
	{

		const kuka::Transform& tranPose = data.pose();
		const kuka::Transform& tranForce = data.force();
		const std::string& msg = data.msg();




		px = tranPose.x();
		py = tranPose.y();
		pz = tranPose.z();
		pa = tranPose.a();
		pb = tranPose.b();
		pc = tranPose.c();

		fx = tranForce.x();
		fy = tranForce.y();
		fz = tranForce.z();
		fa = tranForce.a();
		fb = tranForce.b();
		fc = tranForce.c();

		controltype = std::stoi(msg.c_str());
	}
	else
	{
		px = 1e-9;
		py = 1e-9;
		pz = 1e-9;
		pa = 1e-9;
		pb = 1e-9;
		pc = 1e-9;

		fx = 1e-9;
		fy = 1e-9;
		fz = 1e-9;
		fa = 1e-9;
		fb = 1e-9;
		fc = 1e-9;

		controltype = 7; //means it is empty for now
	
	}

}





void Receiver::start()
{
	Qu = new Message_Queue<kuka::Command>();
	active = true;

	//R_thread = std::thread([this] {this->run(); });
	R_thread = boost::thread([this] {this->run(); });
	R_thread.detach();
}

//void Receiver::handler(const boost::system::error_code& error)
//{
//	if (!error)
//	{
//		std::cout << "timer expired" << std::endl;
//		active = false;
//	}
//}

void Receiver::ReceiveMessage(){



	boost::array<char, 118> recv_buf;
	kuka::Command Rcom; //message declaration
	boost::system::error_code error;
	std::string mex;




	try{
		//std::cout << "receiving" << std::endl;
	//	boost::asio::deadline_timer timer(io_service);
	//	timer.expires_from_now(boost::posix_time::milliseconds(10)); //to give in input?
	//	timer.async_wait(handler);
		//udp::endpoint remote_endpoint;
		size_t len = socket.receive_from(boost::asio::buffer(recv_buf), uep, 0, error); //reading from socket and saving length


		if (error && error != boost::asio::error::message_size)
			throw boost::system::system_error(error); //some other errors


		/*DEBUG PRINTING*/
		//std::cout << "recv_buf.data(): " << recv_buf.data() << std::endl;
		/*END DEBUG PRINTING*/

		mex.append(recv_buf.begin(), recv_buf.begin() + len);
		/*DEBUG PRINTING*/
		//std::cout << "mex " << mex << std::endl;
		/*END DEBUG PRINTING*/

		Rcom.ParseFromString(mex);

	}
	catch (std::exception& e)
	{
		std::cout << "not receiving" << std::endl;
		std::cerr << e.what() << std::endl;
		
	}
	Qu->m_push(Rcom);
}

void Receiver::run()
{

	while (active){
		//get data message from the network and turn into a string
		ReceiveMessage();
		// convert kuka::command in Mex_type
		}
}