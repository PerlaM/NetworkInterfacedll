#ifndef NetworkInterface_hpp
#define NetworkInterface_hpp

#ifdef __cplusplus
extern "C" {
#endif


	__declspec(dllexport) int Setup(const char* address, int Sport, int Rport);

	__declspec(dllexport) int SendMessageToRobot(double, double, double, double, double, double, double, double, double, double, double, double, int);

	__declspec(dllexport) int ReceiveMessage(double &px, double &py, double &pz, double &pa, double &pb, double &pc, double &fx, double &fy, double &fz, double &fa, double &fb, double &fc, int &controltype);
	
	__declspec(dllexport) int CleanUp();

#ifdef __cplusplus
}
#endif
#endif

