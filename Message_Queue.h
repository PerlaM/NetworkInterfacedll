#ifndef Message_Queue_hpp
#define Message_Queue_hpp

#include <queue>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>


template<typename T>
class Message_Queue
{
   private: 
	   mutable boost::mutex Q_mutex;
	   boost::condition_variable Q_CondVar;
	   std::queue<T> m_queue;
    public:
		Message_Queue(){};
        
		template<typename T>
		void m_push(T message)
		{
			boost::lock_guard<boost::mutex> lk(Q_mutex);
			m_queue.push(message);
			Q_CondVar.notify_one();
        }

		template<typename T>
		void wait_and_pop(T& message)
		{
			boost::unique_lock<boost::mutex> lk(Q_mutex);
			Q_CondVar.wait(lk, [this]{return !m_queue.empty(); });
			message = m_queue.front();
			m_queue.pop();
        }

		template<typename T>
		boost::shared_ptr<T> wait_and_pop()
		{
			boost::unique_lock<boost::mutex> lk(Q_mutex);
			Q_CondVar.wait(lk, [this]{return !m_queue.empty(); });
			boost::shared_ptr<T> res(boost::make_shared<T>(m_queue.front()));
			m_queue.pop();
			return res;
        }

        template<typename T>
		bool try_pop(T& message)
		{
			boost::lock_guard<boost::mutex> lk(Q_mutex);
			if (m_queue.empty())
				return false;
			message = m_queue.front();
			m_queue.pop();
			return true;
        }

        template<typename T>
		boost::shared_ptr<T> try_pop()
		{
			boost::lock_guard<boost::mutex> lk(Q_mutex);
			if (m_queue.empty())
				return boost::shared_ptr<T>();
			boost::shared_ptr<T> res(boost::make_shared<T>(m_queue.front()));
			m_queue.pop();
			return res;
        }
		
		bool Message_Queue::empty()
		{
		   boost::lock_guard<boost::mutex> lk(Q_mutex);
			return m_queue.empty();
		}

};

#endif